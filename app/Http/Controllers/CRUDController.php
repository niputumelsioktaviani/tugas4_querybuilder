<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CRUDController extends Controller
{
    public function show()
    {
        $karyawan = DB::table('karyawan')->get();
        return view('read', ['k' => $karyawan]);
    }
    public function create()
    {
        return view('create');
    }

    public function prosesSimpan(Request $req)
    {
        DB::table('karyawan')->insert(
            [
                'nama_karyawan' => $req->nama,
                'no_karyawan' => $req->no,
                'no_telp_karyawan' => $req->notlp,
                'jabatan_karyawan' => $req->jabatan,
                'divisi_karyawan' => $req->divisi,
            ]
        );

        return redirect('/datas')->with('status', 'Data Berhasil Dibuat!');
    }

    public function edit($id)
    {
        $datas = DB::table('karyawan')->where('id', $id)->first();
        return view('edit', compact('datas'));
    }

    public function prosesEdit(Request $req, $id)
    {
        DB::table('karyawan')->where('id', $id)
            ->update([
                'nama_karyawan' => $req->nama,
                'no_karyawan' => $req->no,
                'no_telp_karyawan' => $req->notlp,
                'jabatan_karyawan' => $req->jabatan,
                'divisi_karyawan' => $req->divisi,
            ]);

        return redirect('/datas')->with('status', 'Data Berhasil DiUpdate!');
    }

    public function delete($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/datas')->with('status', 'Data Berhasil DiHapus!');
    }
}
