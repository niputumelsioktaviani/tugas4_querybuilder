<?php

use App\Http\Controllers\CRUDController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', [CRUDController::class, 'home'])->name('home');*/

Route::get('/', [CRUDController::class, 'show'])->name('datas');
Route::get('datas', [CRUDController::class, 'show'])->name('datas');
Route::get('create', [CRUDController::class, 'create'])->name('create');
Route::post('datas', [CRUDController::class, 'prosesSimpan'])->name('datas');
Route::get('datas/edit/{id}', [CRUDController::class, 'edit'])->name('datas/edit/{id}');
Route::put('datas/edit/{id}', [CRUDController::class, 'prosesEdit'])->name('datas/edit/{id}');
Route::delete('datas/{id}', [CRUDController::class, 'delete'])->name('datas/{id}');
