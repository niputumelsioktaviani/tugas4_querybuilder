@extends('layout')

@section('judul')
    Create Data
@endsection

@section('konten')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <h3>Create Data Karyawan</h3>
                    </div>
                    <div class="full-right">
                        <a href="{{ url('datas') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-undo"></i> Back Data
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="col-md-6 offset-md-3">
                        <form action="{{ url('datas') }}" method="post">
                            @csrf

                            <div class="form-group">
                                <label>Nama Karyawan</label>
                                <input type="text" name="nama" class="form-control" autofocus required>
                            </div>
                            <div class="form-group">
                                <label>Nomor Karyawan</label>
                                <input type="text" name="no" class="form-control" autofocus required>
                            </div>
                            <div class="form-group">
                                <label>Nomor Telp Karyawan</label>
                                <input type="text" name="notlp" class="form-control" autofocus required>
                            </div>
                            <div class="form-group">
                                <label>Jabatan Karyawan</label>
                                <input type="text" name="jabatan" class="form-control" autofocus required>
                            </div>
                            <div class="form-group">
                                <label>Divisi Karyawan</label>
                                <input type="text" name="divisi" class="form-control" autofocus required>
                            </div>
                            <button type="submit" class="btn btn-success">Save Data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
