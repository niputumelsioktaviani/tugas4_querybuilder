@extends('layout')

@section('judul')
    Update Data
@endsection

@section('konten')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <h3>Update Data Karyawan</h3>
                    </div>
                    <div class="full-right">
                        <a href="{{ url('datas') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-undo"></i> Back Data
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <form action="{{ url('datas/edit/' . $datas->id) }}" method="post">
                                @method('put')
                                @csrf

                                <div class="form-group">
                                    <label>Nama Karyawan</label>
                                    <input type="text" name="nama" class="form-control"
                                        value="{{ $datas->nama_karyawan }}">
                                </div>
                                <div class="form-group">
                                    <label>Nomor Karyawan</label>
                                    <input type="text" name="no" class="form-control" value="{{ $datas->no_karyawan }}">
                                </div>
                                <div class="form-group">
                                    <label>Nomor Telp Karyawan</label>
                                    <input type="text" name="notlp" class="form-control"
                                        value="{{ $datas->no_telp_karyawan }}">
                                </div>
                                <div class="form-group">
                                    <label>Jabatan Karyawan</label>
                                    <input type="text" name="jabatan" class="form-control"
                                        value="{{ $datas->jabatan_karyawan }}">
                                </div>
                                <div class="form-group">
                                    <label>Divisi Karyawan</label>
                                    <input type="text" name="divisi" class="form-control"
                                        value="{{ $datas->divisi_karyawan }}">
                                </div>
                                <button type="submit" class="btn btn-success">Update Data</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
