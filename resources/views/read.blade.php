@extends('layout')

@section('judul')
    Read Data
@endsection

@section('konten')
    <div class="content mt-3">
        <div class="animated fadeIn">
            @if (session('status'))
                <div class="alert alert-warning">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <h3>Read Data Karyawan</h3>
                    </div>
                    <div class="full-right">
                        <a href="/create" class="btn btn-success btn-sm">
                            <i class="fa fa-plus"></i> Create Data
                        </a>
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th>No.</th>
                                <th>Nama Karyawan</th>
                                <th>Nomor Karyawan</th>
                                <th>Nomor Telp Karyawan</th>
                                <th>Jabatan Karyawan</th>
                                <th>Divisi Karyawan</th>
                                <th>Action</th>
                            </tr>

                        <tbody>
                            @foreach ($k as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nama_karyawan }}</td>
                                    <td>{{ $item->no_karyawan }}</td>
                                    <td>{{ $item->no_telp_karyawan }}</td>
                                    <td>{{ $item->jabatan_karyawan }}</td>
                                    <td>{{ $item->divisi_karyawan }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('datas/edit/' . $item->id) }}" class="btn btn-primary"> Edit
                                        </a>
                                        <form action="{{ url('datas/' . $item->id) }}" method="POST">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-danger btn-sm"> Hapus
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
